//
//  HarmonicAnalyser.swift
//  harmonic
//
//  Created by clar3 on 05/10/2018.
//  Copyright © 2018 Ashla. All rights reserved.
//

import Foundation
import TradingCore

public struct HarmonicPattern: Codable {
    var ab_move : Range<Double>
    var bc_move : Range<Double>
    var cd_move : Range<Double>
    var name : String!
    var direction : Int
    
    public var fullName: String {
        return "\((direction == 0 ? "bullish" : "bearish")) \(name!)"
    }
    
    public var bullish: Bool {
        return direction == 0
    }
}

public class HarmonicPatterns {
    static func rng(_ a: Double,_ b: Double) -> Range<Double> {
        return Range(uncheckedBounds: (a, b))
    }
    
    static func gartley(_ direction: Int) -> HarmonicPattern {
    
        return HarmonicPattern(ab_move: rng(0.618, 0.618), bc_move: rng(0.382, 0.886), cd_move: rng(1.272, 1.618), name: "Gartley", direction: direction)
    }
    static func crab(_ direction: Int) -> HarmonicPattern {
        return HarmonicPattern(ab_move: rng(0.382, 0.618), bc_move: rng(0.382, 0.886), cd_move: rng(2.24, 3.618), name: "Crab", direction: direction)
    }
    static func bat(_ direction: Int) -> HarmonicPattern {
        return HarmonicPattern(ab_move: rng(0.382, 0.500), bc_move: rng(0.382, 0.886), cd_move: rng(1.618, 2.618), name: "Bat", direction: direction)
    }
    static func butterfly(_ direction: Int) -> HarmonicPattern {
        return HarmonicPattern(ab_move: rng(0.786, 0.786), bc_move: rng(1.27, 1.618), cd_move: rng(1.618, 2.618), name: "Butterfly", direction: direction)
    }
    
    static public func getAllPatterns() -> [HarmonicPattern] {
        return [0, 1].map { [crab($0), bat($0), butterfly($0), gartley($0)] }.flatMap { $0 }
    }
}

public class HarmonicAnalyser {
    public static func window<T>(_ input : [T], size : Int, execute : ([T], Int) -> Void ) {
        for i in 1...input.count {
            if i - size >= 0 {
                let window = Array(input[i - size..<i])
                execute(window, i)
            }
        }
    }
  
    public static var extremaAnchor = {(_ bar: PriceAction,_ isLow: Bool) -> Double in
        return bar.average()
    }
    static func extremaOnWindow(window: [PriceAction], j: Int,_ mins: [Int]? = nil,_ maxs: [Int]? = nil) -> (Int, Bool)? {
        let cut = Int(ceil(Double(window.count) / 2)) - 1
        let i = j - window.count
        let min_vals = window.map{ extremaAnchor($0, true) }
        let max_vals = window.map{ extremaAnchor($0, false) }
        if let min = min_vals.min(), let max = max_vals.max() {
            let ind = i + cut
            if min == extremaAnchor(window[cut], true) {
                if mins == nil || mins!.last != ind {
                    return (ind, true)
                }
            } else if max == extremaAnchor(window[cut], false) {
                if maxs == nil || maxs!.last != ind {
                    return (ind, false)
                }
            }
        }
        return nil
    }
    
    static func extremaTailEnd(_ input: [PriceAction], windowSize: Int) -> (Int, Int)? {
        let halfSize = Int(ceil(Double(windowSize) / 2))
        let tailEnd = Array(input[(input.count - halfSize)..<input.count])
        let min_vals = tailEnd.map{ extremaAnchor($0, true) }
        let max_vals = tailEnd.map{ extremaAnchor($0, false) }
        if let max = max_vals.max(), let min = min_vals.min() {
            var minVal: Int?
            var maxVal: Int?
            for (i, val) in tailEnd.enumerated() {
                if extremaAnchor(val, false) == max {
                    maxVal = i + input.count - halfSize
                }
                if extremaAnchor(val, true) == min {
                    minVal = i + input.count - halfSize
                }
            }
            if let maxVal = maxVal, let minVal = minVal {
                return (maxVal, minVal)
            }
        }
        return nil
    }
    
    static func extractExtrema(_ input : [PriceAction], windowSize: Int = 15, coalesce: Bool = false) -> ([Int], [Int]){
        var mins : [Int] = []
        var maxs : [Int] = []
        HarmonicAnalyser.window(input, size: windowSize) { (window, j) in
            if let (ind, isMin) = HarmonicAnalyser.extremaOnWindow(window: window, j: j, mins, maxs) {
                if isMin { mins.append(ind) }
                else { maxs.append(ind) }
            }
        }
        
        if let (min, max) = HarmonicAnalyser.extremaTailEnd(input, windowSize: windowSize) {
            mins.append(min)
            maxs.append(max)
        }
        
        return (mins, maxs)
    }
    
    public static var bullishWeight : Double = 1.1
    public static var bearishWeight : Double = 0.9
    public static var errorAllowed  = { (frame: TimeFrame, bullish: Bool) -> Double in
        var rawVal: Double!
        switch frame {
        case .sixtyMinute, .thirtyMinute, .fifteenMinute:
            rawVal = 0.15
        case .daily, .weekly:
            rawVal = 0.3
        default:
            rawVal = 0.05
        }
        return bullish ? rawVal * bullishWeight : rawVal * bearishWeight
    }
    
    static func checkHarmonic(_ prices : [PriceAction], for pattern : HarmonicPattern, frame: TimeFrame) -> Bool {
        
        let bullish = pattern.bullish
        guard prices.count == 5 else { return false }
        
        let xa = prices[1].average() - prices[0].average()
        let ab = prices[2].average() - prices[1].average()
        let bc = prices[3].average() - prices[2].average()
        let cd = prices[4].average() - prices[3].average()
        
        let condition = bullish == true ?   (xa > 0 && ab < 0 && bc > 0 && cd < 0) :
                                            (xa < 0 && ab > 0 && bc < 0 && cd > 0)
        if condition {
            let errorBar = errorAllowed(frame, bullish)
            
            let ab_range = getRange(for: pattern.ab_move.lowerBound, and: pattern.ab_move.upperBound, withMoveA: prices[1], B: prices[0], error: errorBar)
            let bc_range = getRange(for: pattern.bc_move.lowerBound, and: pattern.bc_move.upperBound, withMoveA: prices[2], B: prices[1], error: errorBar)
            let cd_range = getRange(for: pattern.cd_move.lowerBound, and: pattern.cd_move.upperBound, withMoveA: prices[3], B: prices[2], error: errorBar)
        
            if ab_range.contains(abs(ab)) && bc_range.contains(abs(bc)) && cd_range.contains(abs(cd)) {
                return true
            }
        }
        
        return false
    }
    
    static func getTPsSLs(_ indicies: [Int], full sourceData: [PriceAction], for pattern: HarmonicPattern, frame: TimeFrame) -> ([Double], [Double]) {
        guard indicies.count == 5 else { return ([], []) }
        let sourcePrices = indicies.map { sourceData[$0] }

        let bullish = pattern.bullish
        let dSwingDirec: Double = bullish  ? -1 : 1
        let errorBar = errorAllowed(frame, bullish)
        let range = getRange(for: pattern.cd_move.lowerBound, and: pattern.cd_move.upperBound, withMoveA: sourcePrices[3], B: sourcePrices[2], error: errorBar)
        
        let prices = sourcePrices.map { $0.average() }
        let bc = prices[3] - prices[2]
        let lower = prices[3] + dSwingDirec * range.lowerBound
        let upper = prices[3] + dSwingDirec * range.upperBound
        let classicTP = prices[3] + -1 * (bc * 0.618)
        var tps = [classicTP, prices[2], prices[3]]
        var sls = [upper]
        
        
        let extra = 5
        let structureRange = max(indicies.first! - extra, 0)...min(indicies.last! + extra, sourceData.count - 1)
        let structure = HarmonicAnalyser.reportStructure(prices: sourceData, uptill: indicies.last!).filter { $0.value.1 > 2 && structureRange.contains($0.value.0) }
        
        for (_, (start, _)) in structure {
            let top = max((start - extra), 0)
            var out: Double?
            let choose = { i in bullish ? sourceData[i].low : sourceData[i].high }
            for i in (top...start).reversed() {
                out = out == nil ? choose(i) : (
                    bullish ? min(out!, choose(i)) : max(out!, choose(i))
                )
            }
            out! += 0.001 * dSwingDirec
            if (bullish && prices[4] > out!) || (!bullish && prices[4] < out!) {
                sls.append(out!)
            }
        }
        
        if (bullish && prices[4] < lower) || (!bullish && prices[4] > lower) {
            tps.append(lower)
        }
        
        func order(_ list: [Double]) -> [Double] {
            let sorted = list.sorted()
            return bullish ? sorted : sorted.reversed()
        }
        
        
        (tps, sls) = (order(tps), order(sls).reversed())

        return (tps, sls)
    }
    
    static func getRange(for movement : Double, and option : Double, withMoveA a : PriceAction, B b: PriceAction, error errorAllowed: Double) -> Range<Double> {
        let getMax = max(abs(a.high - b.low), abs(a.low - b.high))
        return Range(uncheckedBounds: (lower: (movement - errorAllowed) * getMax, upper: (option + errorAllowed) * getMax))
    }
    
    static func checkStructure( window : [PriceAction] ) -> Double? {
        guard window.count == 2 else { return nil }
    
        if (window[0].open > window[0].close && window[1].open < window[1].close) || (window[0].open < window[0].close && window[1].open > window[1].close) {
            return ( window[0].close + window[1].open ) / 2
        }
        
        return nil
    }
    
    public static func reportStructure( prices : [PriceAction], uptill cutoffX: Int? = nil ) -> [Double : (Int, Int)] {
        
        func roundToPlaces(value:Double, places:Int) -> Double {
            let divisor = pow(10.0, Double(places))
            return round(value * divisor) / divisor
        }
        
        var dict = [Double : (Int, Int)]()
        let fraction = cutoffX == nil || cutoffX! >= prices.count ? prices : Array(prices[0...cutoffX!])
        HarmonicAnalyser.window(fraction, size: 2) { (window, i) in
            if let at = checkStructure(window: window) {
                let key = roundToPlaces(value: at, places: 2)
                dict[key] = (i, (dict[key]?.1 ?? 0) + 1)
            }
        }
        return dict
        
    }
    
    public static func sma( window : [Double] ) -> Double {
        return window.reduce(0, +) / Double(window.count)
    }
    
    public static func ema(of new : Double, given previous : Double, with period : Double ) -> Double {
        let multiplier = 2.0 / (period + 1.0)
        return (new - previous) * multiplier + previous
    }
}
