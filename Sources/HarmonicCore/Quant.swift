//
//  ViewController+Analysis.swift
//  harmonic
//
//  Created by clar3 on 31/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//
import Foundation
import TradingCore

public protocol QuantUI {
    func updatedStore(forPair pair: CurrencyPair, andFrame frame: TimeFrame)
    func notify(_ tip: StockTip, updated: Bool)
    func canReconfigure(continue: () -> Void) -> Void
}

public protocol QuantTrader {
    func trade(order: TradeOrder) -> Void
}

extension CurrencyPair {
    public var slippage: Double {
        switch self {
        case .AUD_USD: return (1.5 * 0.5 * 0.0001)
        case .CAD_JPY: return (3.5 * 0.5 * 0.01)
        case .EUR_CAD: return (3.4 * 0.5 * 0.0001)
        case .EUR_CHF: return (2.6 * 0.5 * 0.0001)
        case .EUR_GBP: return (1.6 * 0.5 * 0.0001)
        case .EUR_JPY: return (1.7 * 0.5 * 0.01)
        case .EUR_NZD: return (8.2 * 0.5 * 0.0001)
        case .EUR_USD: return (1.2 * 0.5 * 0.0001)
        case .GBP_AUD: return (5.9 * 0.5 * 0.0001)
        case .GBP_CAD: return (5.2 * 0.5 * 0.0001)
        case .GBP_CHF: return (4.5 * 0.5 * 0.0001)
        case .GBP_JPY: return (3.3 * 0.5 * 0.01)
        case .GBP_NZD: return (7.5 * 0.5 * 0.0001)
        case .GBP_USD: return (1.8 * 0.5 * 0.0001)
        case .NZD_USD: return (3 * 0.5 * 0.0001)
        case .USD_CAD: return (1.9 * 0.5 * 0.0001)
        case .USD_CHF: return (2 * 0.5 * 0.0001)
        case .USD_JPY: return (1.3 * 0.5 * 0.01)
        }
    }
    public var volatile_slippage: Double {
        switch self {
        case .AUD_USD: return (15 * 0.5 * 0.0001)
        case .CAD_JPY: return (24 * 0.5 * 0.01)
        case .EUR_CAD: return (24 * 0.5 * 0.0001)
        case .EUR_CHF: return (24 * 0.5 * 0.0001)
        case .EUR_GBP: return (15 * 0.5 * 0.0001)
        case .EUR_JPY: return (15 * 0.5 * 0.01)
        case .EUR_NZD: return (24 * 0.5 * 0.0001)
        case .EUR_USD: return (13 * 0.5 * 0.0001)
        case .GBP_AUD: return (24 * 0.5 * 0.0001)
        case .GBP_CAD: return (24 * 0.5 * 0.0001)
        case .GBP_CHF: return (24 * 0.5 * 0.0001)
        case .GBP_JPY: return (24 * 0.5 * 0.01)
        case .GBP_NZD: return (24 * 0.5 * 0.0001)
        case .GBP_USD: return (15 * 0.5 * 0.0001)
        case .NZD_USD: return (15 * 0.5 * 0.0001)
        case .USD_CAD: return (15 * 0.5 * 0.0001)
        case .USD_CHF: return (15 * 0.5 * 0.0001)
        case .USD_JPY: return (15 * 0.5 * 0.01)
        }
    }
}

public class Quant {
    
    public var uiDelegate: QuantUI?
    public var trader: QuantTrader?
    public var chosenPairs = CurrencyPair.allPairs
    public var analyseFrames = [TimeFrame.weekly, TimeFrame.daily, TimeFrame.sixtyMinute, TimeFrame.thirtyMinute]
    public var chosenFrames = [TimeFrame.weekly, TimeFrame.daily, TimeFrame.sixtyMinute, TimeFrame.thirtyMinute, TimeFrame.fifteenMinute, TimeFrame.fiveMinute]
    
    public var slippageAdjust = { (pair: CurrencyPair) -> Double in
        return pair.volatile_slippage * 0.5
    }
    
    public func kickOff() {
        if queue.isEmpty {
            addAllToQueue()
            next()
        }
    }
    
    public func getData(forPair selectedPair: CurrencyPair, andFrame selectedFrame: TimeFrame) -> [PriceAction] {
        return get(forPair: selectedPair, andFrame: selectedFrame, in: self.dataStore)
    }
    public func getDiscovery(forPair selectedPair: CurrencyPair, andFrame selectedFrame: TimeFrame) -> [CachedDiscovery] {
        return get(forPair: selectedPair, andFrame: selectedFrame, in: self.discoveryStore)
    }
    public var discoveryStore: [String: [CachedDiscovery]] = [:]
    
    
    public static func getPatternName(for key: String) -> String {
        return String(key.split(separator: "-").first ?? "dubious")
    }
    static func getKey(for pattern: HarmonicPattern, entries: [(Int, PriceAction)], frame: TimeFrame, pair: CurrencyPair) -> String {
        let bois = Array(entries[0..<3])
        return "\(pattern.fullName)-\(pair.baseText)-\(frame.baseText)-\(bois.map { $0.1.average() }.format4())-\(bois.map { Double($0.0) }.format4())"
    }
    
    private var queue: [(CurrencyPair, TimeFrame, Date)] = []
    private let dataQueue = DispatchQueue(label: "dataQueue")
    private var dataClient : ForexDataClient!
    private var dataStore: [String: [PriceAction]] = [:]
    
    public var LOGS = true
    public var SHOULD_YOYO = true
    public var LAG_TIME = 2
    public func clearDiscoveries() {
        self.previousExtrema = nil
        self.discoveryStore = [:]
        for (i, (pair, frame, _)) in queue.enumerated() {
            queue[i] = (pair, frame, Date())
        }
    }
    
    public init(_ forceAlpha : Bool = false, _ broker: TradingClient? = nil) {
        if forceAlpha {
            dataClient = AlphaVantageClient()
        } else if let broker = broker {
            dataClient = broker
        } else {
            do {
                dataClient = try TradingClient()
            } catch {
                dataClient = AlphaVantageClient()
            }
        }
    }
    
    public func getTradingClient() -> TradingClient? {
        return dataClient as? TradingClient
    }
    
    
    private func set<T>(_ val : [T], forPair pair: CurrencyPair, andFrame frame: TimeFrame, in dict: inout [String: [T]]) {
        dataQueue.sync {
            let str = pair.baseText + frame.baseText
            dict[str] = val
        }
    }
    
    private func get<T>(forPair pair: CurrencyPair, andFrame frame: TimeFrame, in dict: [String: [T]]) -> [T] {
        dataQueue.sync {
            let str = pair.baseText + frame.baseText
            return dict[str] ?? []
        }
    }
    
    
    private func scanForHarmonicTrade(with pair: CurrencyPair, frame: TimeFrame) {
        if LOGS {
            print("SCANNING \(pair.baseText) in FRAME \(frame.baseText)")
        }
        let chosenInstrument = pair
        let chosenFrame = frame
        let outputType: OutputType = (frame == .daily || frame == .weekly) ? .compact : .full
        
        let analyse = { (source: [PriceAction]) in
            if self.analyseFrames.contains(chosenFrame) {
                self.analyse(data: source, for: chosenInstrument, in: chosenFrame)
            } else {
                self.next()
            }
        }
        let onMainData: ([CurrencyPair : [PriceAction]]?) -> Void = { result in
            guard let result = result else {
                print("\(Date()) FAILED COLLECTION : VC")
                self.next()
                return
            }
            if self.LOGS {
                print("\(Date()) DATA FOR \(chosenInstrument.rawValue) RECEIVED")
            }
            let keys = Array(result.keys)
            guard let source = result[keys[0]] else { return }
            self.set(source, forPair: chosenInstrument, andFrame: chosenFrame, in: &self.dataStore)
            analyse(source)
        }
        self.dataClient.getDataFor(pair: chosenInstrument, at: chosenFrame, forSize: outputType, forType: .mid, onMainData)
    }
    
    func optWindow<T>(_ input : [T], size : Int, execute : ([T]) -> Void ) {
        for i in 1...input.count {
            if i - size >= 0 {
                let window = Array(input[i - size..<i])
                execute(window)
            }
        }
        let total = input.count - 1
        var secondWindow = Array(input[total - size..<total])
        secondWindow[size - 1] = input.last!
        execute(secondWindow)
    }
    
    public var harmonicOrder: [HarmonicPattern]?
    public var windowSize: Int = 15
    private func checkHarmonicPatterns(in data: [PriceAction], frame: TimeFrame, goNext: Bool = true, windowSize: Int, coalesce: Bool) -> [([(Int, PriceAction)], HarmonicPattern)] {
        let extrema = getExtrema(in: data, goNext: goNext, windowSize: windowSize, coalesce: coalesce)
        var output = [([(Int, PriceAction)], HarmonicPattern)]()
        if extrema.count > 5 {
            optWindow(extrema, size: 5, execute: { window in
                let weekday = data[window.first!].date.weekday
                guard weekday != 2 else { return } // don't trade any patterns forming on monday
                let prices = window.map { data[$0] }
                for pattern in harmonicOrder ?? HarmonicPatterns.getAllPatterns() {
                    if HarmonicAnalyser.checkHarmonic(prices, for: pattern, frame: frame) {
                        let rawPointEntries = window.map{ i -> (Int, PriceAction) in
                            return (i, data[i])
                            
                        }
                        output.append((rawPointEntries, pattern))
                        break
                    }
                }
            })
        }
        return output
    }
    
    var previousExtrema: ([Int], [Int])?
    // doesn't work with coealesce
    private func getExtrema(in data: [PriceAction], goNext: Bool = true, windowSize: Int, coalesce: Bool) -> [Int] {
        guard !goNext && previousExtrema != nil else {
            previousExtrema = HarmonicAnalyser.extractExtrema(data, windowSize: windowSize, coalesce: coalesce)
            return (previousExtrema!.0 + previousExtrema!.1).sorted()
        }

        var needUpdate = false
        previousExtrema!.0 = previousExtrema!.0.map { $0 - 1 }.filter { $0 >= 0 }
        previousExtrema!.1 = previousExtrema!.1.map { $0 - 1 }.filter { $0 >= 0 }

        let tailEndMin = previousExtrema!.0.removeLast()
        let tailEndMax = previousExtrema!.1.removeLast()
        let lastWindow = Array(data[(data.count - windowSize)...])
        if let (newMiddle, isMin) = HarmonicAnalyser.extremaOnWindow(window: lastWindow, j: data.count) {
            if isMin {
                previousExtrema!.0.append(newMiddle)
            } else {
                previousExtrema!.1.append(newMiddle)
            }
            needUpdate = true
        }
        
        if  let (min, max) = HarmonicAnalyser.extremaTailEnd(data, windowSize: windowSize) {
            previousExtrema!.0.append(min)
            previousExtrema!.1.append(max)
            needUpdate = needUpdate || (min != tailEndMin || max != tailEndMax)
        }

        return needUpdate ? (previousExtrema!.0 + previousExtrema!.1).sorted().suffix(7) : []
    }
    
    public func analyse(data sourceData: [PriceAction], for chosenInstrument: CurrencyPair, in chosenFrame: TimeFrame, goNext: Bool = true) {
        
        // check for harmonics
        let discoveredHarmonics = checkHarmonicPatterns(in: sourceData, frame: chosenFrame, goNext: goNext, windowSize: windowSize, coalesce: false).reversed()
        guard discoveredHarmonics.count > 0 else { if goNext { next() }; return } // no tradeable harmonics found so skip
        
        var discoveries: [CachedDiscovery] = []
        let previous = get(forPair: chosenInstrument, andFrame: chosenFrame, in: self.discoveryStore)
        for (rawEntries, pattern) in discoveredHarmonics {
            let bullish = pattern.direction == 0
            let rawActions = rawEntries.map { $0.1 }
            let rawInds = rawEntries.map { $0.0 }
            let endPattern = rawEntries.last!.1
            let lastInd = rawEntries.last!.0
            let lastY = endPattern.average()
            let (rawTps, rawSls) = HarmonicAnalyser.getTPsSLs(rawInds, full: sourceData, for: pattern, frame: chosenFrame)
            let slippage = self.slippageAdjust(chosenInstrument)
            let OGtps = rawTps.map { $0 + (bullish ? slippage : -slippage)  }
            let OGsls = rawSls.map { $0 + (bullish ? -slippage : slippage)  }
            let patternDate = sourceData[lastInd].date
            let aim = OGtps[2]
            let entry = (aim - lastY) * 0.05 + lastY
            let currentSL = OGsls.first! // maybe used later on so var
            let diffCalc = { x in (x - entry) * (bullish ? 1 : -1) }
            let calcResult = { x in diffCalc(x) } // don;t try do currency conversion leave pips
            let risk = calcResult(currentSL)
            let patternKey = Quant.getKey(for: pattern, entries: rawEntries, frame: chosenFrame, pair: chosenInstrument)
            
            
            let rawDiscovery = CachedDiscovery(instrument: chosenInstrument,
                                               frame: chosenFrame,
                                               rawPatternData: rawActions,
                                               patternKey: patternKey,
                                               entry: entry,
                                               currentSL: currentSL,
                                               tps: OGtps,
                                               sls: OGsls,
                                               result: nil,
                                               risk: risk,
                                               startDate: patternDate,
                                               endDate: nil)
            for discovery in discoveries {
                if rawDiscovery == discovery {
                    continue
                }
            }
            
            discoveries.append(rawDiscovery)
            var updated = false
            for lhs in previous {
                if lhs.instrument == chosenInstrument && lhs.frame == chosenFrame && lhs.patternKey == patternKey {
                    updated = true
                    break
                }
            }
            
            if !updated {
                if lastInd >= sourceData.count - 3, let fsl = OGsls.first { // it's a latest boiiiiii send it off
                    let tradeOrer = TradeOrder(startDate: patternDate, pair: chosenInstrument, frame: chosenFrame, entries: rawActions, bullish: pattern.bullish, patternKey: patternKey, entry: entry, sl: fsl, tps: OGtps)
                    trader?.trade(order: tradeOrer)
                    //print("TRADING \(patternKey) BY \(sourceData.count - 1 - lastInd) ON \(patternDate)")
                } else {
                    //print("FOUND BUT LATE: \(patternKey) BY \(sourceData.count - 1 - lastInd) ON \(patternDate) inds \(rawEntries.map { $0.ind! })")
                }
                
            } else {
                print("UPDATED \(patternKey); \(patternDate)")
            }
        }
        
        set(discoveries, forPair: chosenInstrument, andFrame: chosenFrame, in: &discoveryStore)
        
        if goNext {
            next()
        }
    }

    private func addAllToQueue() {
        for frame in chosenFrames {
            for pair in chosenPairs {
                queue.append((pair, frame, Date()))
            }
        }
    }
    private func pullFromQueue() -> (CurrencyPair, TimeFrame)? {
        for i in 0..<queue.count {
            let (pair, frame, date) = queue[i]
            let diff =  date.timeIntervalSinceNow
            if diff <= 0 {
                queue[i] = (pair, frame, Date(timeIntervalSinceNow: frame.secondValue))
                return (pair, frame)
            }
        }
        return nil
    }
    private func next() {
        if let (pair, frame) = pullFromQueue() {
            DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 0.1) {
                self.scanForHarmonicTrade(with: pair, frame: frame)
            }
        } else {
            print("--------WAITING TO RESCAN--------")
            if let uiDelegate = uiDelegate { uiDelegate.canReconfigure(continue: self.next) }
            else {
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + (1 * 60)) {
                    self.next()
                }
            }
        }
    }
    
}
